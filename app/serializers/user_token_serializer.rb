class UserTokenSerializer < ActiveModel::Serializer
  attributes :auth_token
end