class ApplicationController < ActionController::Base
  #TODO: Move this to application.rb as config.force_ssl = true
  force_ssl if Rails.env.to_sym == :production
end
