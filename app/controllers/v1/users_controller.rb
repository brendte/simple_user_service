module V1
  class UsersController < ApplicationController
    include PeerValidatable
    include UserTokenValidatable

    respond_to :json

    def show
      if @current_user.blank?
        respond_with({error: 'A user with that id was not found'}, status: :not_found)
      else
        respond_with(@current_user)
      end
    end

    def create
      @user = User.new(params[:user])
      if @user.save
        respond_with(@user, location: nil)
      else
        respond_with({error: @user.errors}, status: :not_found, location: nil)
      end
    end

  end
end