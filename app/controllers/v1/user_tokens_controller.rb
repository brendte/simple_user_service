class V1::UserTokensController < ApplicationController
  include PeerValidatable
  include UserPasswordValidatable
  include UserTokenValidatable

  respond_to :json

  def show
    if @current_user.blank?
      respond_with({error: 'A user was not found'}, status: :unauthorized)
    else
      respond_with(@current_user.user_token)
    end
  end

end
