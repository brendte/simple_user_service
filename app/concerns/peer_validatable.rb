#Include in any controller that needs to protect against access by unknown/unauthorized peer systems
module PeerValidatable
  extend ActiveSupport::Concern

  included do
    before_filter :validate_peer
  end

  private

    def validate_peer
      peer_token = request.headers['X-PEER-TOKEN']
      validator = SystemValidator.instance
      head :unauthorized unless validator.validate_peer(peer_token)
    end

end