#Include in any controller that needs to protect against access by unknown/unauthorized users by checking a user token
module UserTokenValidatable
  extend ActiveSupport::Concern

  included do
    before_filter :validate_and_set_user_with_token
  end

  private

    def validate_and_set_user_with_token
      if @current_user.blank?
        @current_user = authenticate_with_http_token do |token, _|
          User.validate_and_find_with_token(token)
        end
      end
    end

end