#Include in any controller that needs to protect against access by unknown/unauthorized users by checking a user token
module UserPasswordValidatable
  extend ActiveSupport::Concern

  included do
    before_filter :validate_and_set_user_with_password
  end

  private

    def validate_and_set_user_with_password
      if @current_user.blank?
        @current_user = User.validate_and_find_with_email_and_password(request.headers['X-USER'], request.headers['X-PASSWORD']) if request.headers['X-USER'] && request.headers['X-PASSWORD']
      end
    end
end