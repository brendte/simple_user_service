# Include in any "user" model that should have password auth
# In order to use this, you need to run this migration:
#
#class AddUserIdToUserToken < ActiveRecord::Migration
#  def change
#    change_table :user_tokens do |t|
#      t.integer :user_id
#    end
#  end
#end

module UserHasPassword
  extend ActiveSupport::Concern

  included do
    has_secure_password
    attr_accessible :password, :password_confirmation
  end

  module ClassMethods
    def validate_and_find_with_email_and_password(email, password)
      user = User.where(email: email).first
      user && user.authenticate(password) ? user : nil
    end
  end

end