module UserHasThirdParty
  extend ActiveSupport::Concern

  included do

    has_many :third_parties, dependent: :destroy

    def add_third_party(third_party_name, options = {})
      third_party = ThirdParty.validate_and_create(third_party_name, options)
      third_parties << third_party unless third_party.blank?
    end

  end
end