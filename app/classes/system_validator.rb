class SystemValidator
  include Singleton

  def validate_peer(peer_token)
    # We check that ENV['PEER_TOKEN'] is set, so that if it isn't we default to allowing no access instead of complete access
    !ENV['PEER_TOKEN'].blank? && peer_token == ENV['PEER_TOKEN']
  end

end