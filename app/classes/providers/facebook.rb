module Providers
  class Facebook < AuthTwo

    def initialize(values = {})
      super(values)
      @raw_values = values.symbolize_keys
      sanitize_and_validate
      # if this provider has additional fields that need to be passed to ThirdParty::create, uncomment
      # the next two lines and add them to additional_fields:
      # additional_fields = []
      # @missing_fields.concat(additional_fields)
    end

  end
end