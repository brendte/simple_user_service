module Providers
  module Core
    attr_reader :final_values

    def sanitize_and_validate
      sanitized = sanitize
      @final_values = valid? ? sanitized : {}
    end

    private

      def sanitize
        sanitized = {}
        @raw_values.each do |k, v|
          key = k.downcase
          sanitized[key] = v unless @missing_fields.delete(key).blank?
        end
      end

      def valid?
        @missing_fields.length == 0
      end
  end
end