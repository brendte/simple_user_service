module Providers
  class AuthTwo
    include ::Providers::Core

    @required_fields = [:provider_id, :token]

    class << self
      attr_reader :required_fields
    end

    def initialize(values)
      @missing_fields = AuthTwo.required_fields.clone
    end

  end
end