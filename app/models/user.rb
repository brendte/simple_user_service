class User < ActiveRecord::Base
  include UserHasPassword
  include UserHasToken
  include UserHasThirdParty

  before_create :generate_uuid
  after_validation :log_errors, :if => Proc.new { |m| m.errors }

  validates :email, :presence => true, :uniqueness => true
  validates :first_name, :presence => true
  validates :last_name, :presence => true

  attr_accessible :first_name, :last_name, :email

  private

    def log_errors
      Rails.logger.debug self.errors.full_messages.join("\n")
    end

    def generate_uuid
      self.uuid = SecureRandom.uuid # if self.uuid.blank?
    end
end
