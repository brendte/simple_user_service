class UserToken < ActiveRecord::Base
  before_create :generate_access_token
  after_find :reset_if_expired

  belongs_to :user
  #TODO: Add scope to only return auth_token if not expired

  def expired?
    self.auth_token_expiration < DateTime.current
  end

  private

    def generate_access_token
      begin
        self.auth_token = SecureRandom.hex
      end while self.class.exists?(auth_token: auth_token)
      add_token_expiration
    end

    def add_token_expiration
      self.auth_token_expiration = DateTime.current.end_of_day + Rails.configuration.user_token_expires_in_days
    end

    def reset_if_expired
      generate_access_token if self.expired?
      self.save
    end
end
