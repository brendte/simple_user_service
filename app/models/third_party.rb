class ThirdParty < ActiveRecord::Base
  belongs_to :user

  attr_accessible :provider, :provider_id, :token

  def self.supported?(provider)
    Providers.const_defined?(provider.to_sym.capitalize)
  end

  def self.validate_and_create(provider, values = {})
    if supported?(provider)
      my_fb = Providers.const_get(provider.to_sym.capitalize).new(values)
      self.create(my_fb.final_values)
    end
  end
end
