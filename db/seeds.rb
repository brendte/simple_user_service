# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

@admin = User.create(first_name: 'Testy', last_name: 'Smith', email: 'admin@test.com', password: 'please', password_confirmation: 'please')
puts "User #{@admin.first_name} #{@admin.last_name} created."
@user1 = User.create(first_name: 'Testy', last_name: 'Jones', email: 'user1@test.com', password: 'please', password_confirmation: 'please')
puts "User #{@user1.first_name} #{@user1.last_name} created."
