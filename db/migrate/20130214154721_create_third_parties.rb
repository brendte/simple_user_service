class CreateThirdParties < ActiveRecord::Migration
  def change
    create_table :third_parties do |t|
      t.string :provider_id
      t.string :token

      t.timestamps
    end
  end
end
