class AddUserIdToThirdParty < ActiveRecord::Migration
  def change
    add_column :third_parties, :user_id, :integer
  end
end
