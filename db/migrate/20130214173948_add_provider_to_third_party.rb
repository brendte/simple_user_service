class AddProviderToThirdParty < ActiveRecord::Migration
  def change
    add_column :third_parties, :provider, :string
  end
end
