class CreateUserTokens < ActiveRecord::Migration
  def change
    create_table :user_tokens do |t|
      t.string :auth_token, null: false
      t.datetime :auth_token_expiration, null: false

      t.timestamps
    end
  end
end
