class AddUserIdToUserToken < ActiveRecord::Migration
  def change
    change_table :user_tokens do |t|
      t.integer :user_id
    end
  end
end
