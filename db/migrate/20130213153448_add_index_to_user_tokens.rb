class AddIndexToUserTokens < ActiveRecord::Migration
  def change
    add_index :user_tokens, :auth_token
  end
end
