SimpleUserService::Application.routes.draw do
  namespace :v1, defaults: {format: :json} do
    resources :users, only: [:create]
    resource :user, only: [:show]
    resource :user_token, only: [:show]
  end
end
