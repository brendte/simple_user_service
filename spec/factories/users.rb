# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  pwd = Forgery::Basic.password
  factory :user do
    email                   { Forgery::Email.address }
    first_name              'Testy'
    last_name               { Forgery::Name.last_name }
    password                pwd
    password_confirmation   pwd
  end

end
