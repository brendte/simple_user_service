# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :third_party, class: ThirdParty do

    trait :facebook do
      provider    'facebook'
      provider_id 'abc1234'
      token       'xyz5678'
    end

    factory :third_party_facebook_standalone, traits: [:facebook]
    factory :third_party_facebook, class: ThirdParty do
      user
      facebook
    end

  end

end
