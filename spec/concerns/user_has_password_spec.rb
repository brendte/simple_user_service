require 'spec_helper'

describe UserHasPassword do

  before(:each) do
    @user = FactoryGirl.create(:user, {password: 'please', password_confirmation: 'please'})
  end

  describe 'validate_and_find_with_email_and_password' do
    it 'should return a user when the email is valid and password matches' do
      user = User.validate_and_find_with_email_and_password(@user.email, 'please')
      user.uuid.should eq(@user.uuid)
    end

    it 'should not return a user when the email is invalid' do
      user = User.validate_and_find_with_email_and_password('blahbittyblah@blahblahblah.org', 'please')
      user.should be_blank
    end

    it 'should not return a user when the email is valid but the password does not match' do
      user = User.validate_and_find_with_email_and_password(@user.email, 'thank-you')
      user.should be_blank
    end

  end

end