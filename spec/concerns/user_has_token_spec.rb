require 'spec_helper'

describe UserHasToken do

  before(:each) do
    @user = FactoryGirl.create(:user)
  end

  describe 'validate_and_find_with_token' do
    it 'should return a user when the auth_token is valid' do
      user = User.validate_and_find_with_token(@user.user_token.auth_token)
      user.uuid.should eq(@user.uuid)
    end

    it 'should not not return a user when the auth_token is not valid' do
      user = User.validate_and_find_with_token('1234')
      user.should be_blank
    end
  end

end