require 'spec_helper'

describe UserHasThirdParty do

  before(:each) do
    @user = FactoryGirl.create(:user, password: 'please', password_confirmation: 'please')
  end

  describe 'add_third_party' do
    it 'should add a facebook third_party' do
      @user.add_third_party(:facebook, id: ENV['TEST_FB_ID'], token: ENV['TEST_FB_TOKEN'])
      @user.third_parties.first.should_not be_blank
    end
  end

  describe 'find_third_party'

end