require 'spec_helper'
include ApiHelper

describe 'the user_tokens api' do

  before(:each) do
    @users = [FactoryGirl.create(:user, first_name: 'Testy', last_name:'Larue', email: 'testy@tester.com', password: 'please', password_confirmation: 'please')]
  end

  let(:url_base) { '/v1/user_token' }

  context 'getting the token for a request with a valid username and password' do
    subject do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      header('X-USER', 'testy@tester.com')
      header('X-PASSWORD', 'please')
      get url_base
      JSON.parse(last_response.body) unless last_response.body.blank?
    end

    it 'should return a user token message with the correct user token' do
      should eq({"user_token" => {"auth_token" => @users.first.user_token.auth_token}})
    end
  end

  context 'getting the token for a request with a valid token' do
    subject do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      header('X-USER', nil)
      header('X-PASSWORD', nil)
      header('Authorization', "Token token=\"#{@users[0].user_token.auth_token}\"")
      get url_base
      JSON.parse(last_response.body) unless last_response.body.blank?
    end

    it 'should return a user token message with the correct user token' do
      should eq({"user_token" => {"auth_token" => @users.first.user_token.auth_token}})
    end
  end

  context 'getting the token for a request with an invalid username' do
    before(:each) do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      header('X-USER', 'hesty@hester.org')
      header('X-PASSWORD', 'please')
      header('Authorization', nil)
      get url_base
    end

    it 'should return a status 401' do
      last_response.status.should eq(401)
    end

    it 'should not return the error message' do
      body = JSON.parse(last_response.body) unless last_response.body.blank?
      body.should eq({"error"=>"A user was not found"})
    end
  end

  context 'getting the token for a request with an invalid password' do
    before(:each) do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      header('X-USER', 'testy@tester.com')
      header('X-PASSWORD', 'please_me')
      header('Authorization', nil)
      get url_base
    end

    it 'should return a status 401' do
      last_response.status.should eq(401)
    end

    it 'should return the error message' do
      body = JSON.parse(last_response.body) unless last_response.body.blank?
      body.should eq({"error"=>"A user was not found"})
    end
  end

  context 'getting the token for a request with an invalid token' do
    before(:each) do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      header('X-USER', nil)
      header('X-PASSWORD', nil)
      header('Authorization', "Token token=1234")
      get url_base
    end

    it 'should return a status 401' do
      last_response.status.should eq(401)
    end

    it 'should return the error message' do
      body = JSON.parse(last_response.body) unless last_response.body.blank?
      body.should eq({"error"=>"A user was not found"})
    end
  end

  context 'getting the token for a request with with no auth data' do
    before(:each) do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      header('X-USER', nil)
      header('X-PASSWORD', nil)
      header('Authorization', nil)
      get url_base
    end

    it 'should return a status 401' do
      last_response.status.should eq(401)
    end

    it 'should return the error message' do
      body = JSON.parse(last_response.body) unless last_response.body.blank?
      body.should eq({"error"=>"A user was not found"})
    end
  end

end
