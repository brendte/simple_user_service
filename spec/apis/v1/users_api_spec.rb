require 'spec_helper'
include ApiHelper

describe 'the users api' do

  before(:each) do
    @users = [FactoryGirl.create(:user, first_name: 'Testy', last_name:'Larue', email: 'testy@tester.com')]
  end

  let(:url_base) { '/v1/user' }

  context 'getting a known user' do
    subject do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      header('Authorization', "Token token=\"#{@users[0].user_token.auth_token}\"")
      get url_base
      JSON.parse(last_response.body) unless last_response.body.blank?
    end

    it 'should return uuid for Testy Lerue' do
      should eq({"user" => {"uuid" => @users[0].uuid}})
    end
  end

  context 'getting a known user with an invalid peer token' do
    before(:each) do
      header('X-PEER-TOKEN', "#{ENV['PEER_TOKEN']}1234567889")
      header('Authorization', "Token token=\"#{@users[0].user_token.auth_token}\"")
      get url_base
    end

    it 'should return only a header' do
      last_response.body.should be_blank
      last_response.header.should_not be_blank
    end

    it 'should return an http status 401 - Unauthorized' do
      last_response.status.should eq(401)
    end
  end

  context 'getting a known user with a NULL peer token' do
    before(:each) do
      header('X-PEER-TOKEN', nil)
      header('Authorization', "Token token=\"#{@users[0].user_token.auth_token}\"")
      get url_base
    end

    it 'should return only a header' do
      last_response.body.should be_blank
      last_response.header.should_not be_blank
    end

    it 'should return an http status 401 - Unauthorized' do
      last_response.status.should eq(401)
    end
  end

  context 'getting an unknown user' do
    before(:each) do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      header('Authorization', 'Token token=1234')
      get url_base
    end

    it 'should return a status 404' do
      @status = last_response.status
      @status.should eq(404)
    end

    it 'should return a not found error' do
      @response = JSON.parse(last_response.body) unless last_response.body.blank?
      @response.should eq({"error"=>"A user with that id was not found"})
    end
  end

  context 'getting a known user with a valid FB token' do
    it 'should return the user'
  end

  context 'getting a known user with an invalid FB token' do
    it 'should not return a user'
  end

  context 'getting an unknown user when the fb token is valid (fb returns a user, but we do not have a user that matches the returned fb user)' do
    it 'should not return a user'
  end

  context 'getting an unknown user with an invalid FB token' do
    it 'should not return a user'
  end

  context 'getting a known user with a NULL FB token' do
    it 'should not return a user'
  end

  context 'getting a known user with an empty string FB token' do
    it 'should not return a user'
  end

  context 'creating a new user with a password' do
    before(:each) do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      post "#{url_base}s", {user: {first_name: 'Testy', last_name: 'Johnson', email: 'testyj@test.com', password: 'please', password_confirmation: 'please'}}
      @response_body = JSON.parse(last_response.body) unless last_response.body.blank?
    end

    it 'should create Testy Johnson in the db' do
      @testy = User.where(email: 'testyj@test.com').first
      @testy.should_not be_nil
    end

    it 'should return Testy Johnsons uuid' do
      @testy = User.where(email: 'testyj@test.com').first
      @response_body["user"]["uuid"].should eq(@testy.uuid)
    end
  end

  context 'creating a new user with a valid fb token' do
    before(:each) do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      post "#{url_base}s", {user: {first_name: 'Pesty', last_name: 'Johanson', email: 'pestyj@test.com', third_party: {provider: 'facebook', provider_id: ENV['TEST_FB_ID'], token: ENV['TEST_FB_TOKEN']}}}
      @response_body = JSON.parse(last_response.body) unless last_response.body.blank?
    end

    it 'should create Pesty Johanson in the DB'
    it 'should return Pesty Johansons uuid'
  end

  context 'unable to create a user' do
    before(:each) do
      header('X-PEER-TOKEN', ENV['PEER_TOKEN'])
      post "#{url_base}s", {user: {first_name: 'Testy', last_name: 'Johnson', email: 'testyj@test.com', password: 'please', password_confirmation: 'pleaseMe'}}
      @response_body = JSON.parse(last_response.body) unless last_response.body.blank?
    end

    it 'should not create Testy Johnson in the db' do
      @testy = User.where(email: 'testyj@test.com').first
      @testy.should be_nil
    end

    it 'should return error text' do
      @response_body["error"].should eq("password" => ["doesn't match confirmation"])
    end
  end

end
