require 'spec_helper'

describe User do
  describe 'creating a new user' do
    subject do
      FactoryGirl.create(:user)
    end

    its(:uuid) { should_not be_blank }
    its('user_token.auth_token') { should_not be_blank }
    its('user_token.auth_token_expiration') { should_not be_blank }
  end
end
