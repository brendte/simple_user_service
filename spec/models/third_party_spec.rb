require 'spec_helper'   #

describe ThirdParty do


  describe 'creating standalone' do
    subject do
      FactoryGirl.create(:third_party_facebook_standalone)
    end

    its(:provider) { should eq('facebook') }
    its(:provider_id) { should eq('abc1234') }
    its(:token) { should eq('xyz5678') }
  end

  describe 'creating with user' do
    subject do
      FactoryGirl.create(:third_party_facebook)
    end

    its(:provider) { should eq('facebook') }
    its(:provider_id) { should eq('abc1234') }
    its(:token) { should eq('xyz5678') }
    its(:user) { should_not be_blank }

  end
end
