require 'spec_helper'

describe UserToken do

  describe 'creating a stand-alone token' do
    subject do
      FactoryGirl.create(:user_token)
    end

    its(:auth_token) { should_not be_blank }
    its('auth_token_expiration.to_datetime') { should eq(DateTime.current.end_of_day + Rails.configuration.user_token_expires_in_days) }
  end

  describe 'the system noticing an expired token and resetting it' do
    before do
      @user_token = FactoryGirl.create(:user_token)
      @user_token.auth_token_expiration.to_datetime.should eq(DateTime.current.end_of_day + Rails.configuration.user_token_expires_in_days)
      @user_token.auth_token_expiration = DateTime.current.end_of_day - 1
      @user_token.save
      @user_token.auth_token_expiration.to_datetime.should eq(DateTime.current.end_of_day - 1)
      @found_user_token = UserToken.find(@user_token)
    end

    context 'in-memory user token object' do
      subject do
        @found_user_token
      end

      its('auth_token_expiration.to_datetime') { should eq(DateTime.current.end_of_day + Rails.configuration.user_token_expires_in_days) }
      its(:auth_token) { should_not eq(@user_token.auth_token) }
    end

    context 'on-disk user token "object"' do
      subject do
        @found_user_token.reload
      end

      its('auth_token_expiration.to_datetime') { should eq(DateTime.current.end_of_day + Rails.configuration.user_token_expires_in_days) }
      its(:auth_token) { should_not eq(@user_token.auth_token) }
    end
  end
end
