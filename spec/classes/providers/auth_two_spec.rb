require 'spec_helper'

describe Providers::AuthTwo do
  describe 'Providers::AuthTwo @required_fields ' do
    it 'should contain :provider_id, :token' do
      Providers::AuthTwo.required_fields.should eq([:provider_id, :token])
    end
  end
end