require 'spec_helper'

describe Providers::Facebook do
  describe 'validate_and_sanitize' do
    it 'should return a valid hash with all-and-only-the values for ThirdParty::create when all required values are passed in' do
      Providers::Facebook.new({provider_id: '12345', token: '67891'}).sanitize_and_validate.should eq({:provider_id => '12345', :token => '67891'})
    end

    it 'should return a valid hash with symbolized keys when string keys are used' do
      Providers::Facebook.new({'provider_id' => '12345', "token" => '67891'}).sanitize_and_validate.should eq({:provider_id => '12345', :token => '67891'})
    end

    it 'should return an empty hash when all required values are not passed in' do
      Providers::Facebook.new({provider_id: '12345'}).sanitize_and_validate.should eq({})
    end
  end
end