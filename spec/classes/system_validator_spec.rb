require 'spec_helper'

describe SystemValidator do
  describe 'validate_peer' do
    it 'should validate when the token is valid' do
      validator = SystemValidator.instance
      peer_token = ENV['PEER_TOKEN']
      validation_result = validator.validate_peer(peer_token)
      validation_result.should be_true
    end

    it 'should not validate when token is not valid' do
      validator = SystemValidator.instance
      peer_token = "#{ENV['PEER_TOKEN']}1234567889"
      validation_result = validator.validate_peer(peer_token)
      validation_result.should be_false
    end

    it 'should not validate when the token is empty' do
      validator = SystemValidator.instance
      peer_token = ''
      validation_result = validator.validate_peer(peer_token)
      validation_result.should be_false
    end

    it 'should not validate when the token is NULL (nil)' do
      validator = SystemValidator.instance
      peer_token = nil
      validation_result = validator.validate_peer(peer_token)
      validation_result.should be_false
    end

  end
end