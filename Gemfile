source :rubygems
ruby '1.9.3', :engine => 'jruby', :engine_version => '1.7.2'

group :development do
  # run the app using foreman
  # https://github.com/ddollar/foreman
  # make sure to create a Procfile in RAILS_ROOT, for instance (if using Unicorn):
  # web: bundle exec unicorn -p $PORT -c ./config/unicorn.rb
  gem 'foreman', '~> 0.61.0'

  # run your test automatically on save
  # https://github.com/guard/guard
  # on initial bundle run in RAILS_ROOT:
  # $ guard init rspec
  # to run guard, run in RAILS_ROOT:
  # $ guard
  gem 'guard', '~> 1.6.1'
  gem 'guard-rspec', '~> 2.4.0'
  gem 'rb-inotify', '~> 0.8.8', :require => false
  gem 'rb-fsevent', '~> 0.9.3', :require => false
  gem 'rb-fchange', '~> 0.0.6', :require => false
  gem 'ruby_gntp', '~> 0.3.4'

  # use pry as your repl instead of irb when you run
  # $ rails c
  # http://pryrepl.org/
  # https://github.com/rweng/pry-rails
  gem 'pry', '~> 0.9.11.4'
  gem 'pry-rails', '~> 0.2.2'
end # :development

group :test do
  # create test data with factories instead of fixtures
  # https://github.com/thoughtbot/factory_girl_rails
  # https://github.com/thoughtbot/factory_girl
  # https://github.com/thoughtbot/factory_girl/blob/master/GETTING_STARTED.md
  # to use factories in seeds, add to seeds.rb:
  # require 'factory_girl'
  # FactoryGirl.find_definitions rescue true
  # to generate factories in spec/factories, add to RAILS_ROOT/config/application.rb:
  # config.generators do |g|
  #   g.fixture_replacement :factory_girl, :dir => 'spec/factories'
  # end
  gem 'factory_girl_rails', '~> 4.1.0'

  # really the only good way to test apis
  # https://github.com/brynary/rack-test
  gem 'rack-test', '~> 0.6.2', :require => 'rack/test'

  # write cucumber features (in Gherkin) and run via rspec
  # https://github.com/jnicklas/turnip
  # add to .rspec file in RAILS_ROOT:
  # -r turnip/rspec
  # add features to:
  # spec/acceptance/
  # name steps *_steps.rb and add to:
  # spec/acceptance/steps/
  gem 'turnip', '~> 1.1.0'

  ## add some nice matchers for testing of email
  ## https://github.com/bmabey/email-spec/
  ## in spec_helper.rb, add:
  ## require "email_spec"
  ## RSpec.configure do |config|
  ##  config.include(EmailSpec::Helpers)
  ##  config.include(EmailSpec::Matchers)
  ## end
  #gem 'email_spec', '~> 1.4.0'
end # :test

group :production do
  # use postgresql in prod, because it's awesome, and heroku likes it best
  gem 'activerecord-jdbcpostgresql-adapter', '~> 1.2.6'
end  # :production

group :test, :development do
  # use an easy and fast db in dev and test
  gem 'activerecord-jdbcsqlite3-adapter', '~> 1.2.6'

  # test with rspec
  # after initial bundle, run in RAILS_ROOT:
  # $ rails g rspec:install
  gem 'rspec-rails', '~> 2.12.2'

  # write awesomer specs with shoulda matchers
  gem 'shoulda-matchers', '~> 1.4.2'

  # easily create random but correct test data
  # https://github.com/sevenwire/forgery
  # after initial bundle, run in RAILS_ROOT:
  # $ rails g forgery
  gem 'forgery', '~> 0.5.0'

  # simplecov for code coverage
  # https://github.com/colszowka/simplecov
  # add to .gitignore:
  # coverage
  # add to spec_helper at very top:
  # require 'simplecov'
  # SimpleCov.start 'rails'
  gem 'simplecov', '~> 0.7.1'

end # :test, :development

group :production, :development do
  # Use Trinidad/Tomcat to run app
  #gem 'trinidad', '~> 1.4.4'
  # Use puma to run app
  gem 'puma', '~> 1.6.3'
  # New Relic
  gem 'newrelic_rpm', '~> 3.5.5.38'
end # :production, :development

# ALL ENVIRONMENTS

# rails
gem 'rails', '3.2.12'

#activemodel serializers
gem 'active_model_serializers', '~> 0.6.0'

# config and ENV management
# after initial bundle, run in RAILS_ROOT:
# $ rails g figaro:install
gem 'figaro', '~> 0.5.2'

# To use ActiveModel has_secure_password
gem 'bcrypt-ruby', '~> 3.0.0'

# Fix json gem vulnerability
gem 'json', '~> 1.7.7'
